import 'es6-promise/auto'
import Vue from 'vue'
import Meta from 'vue-meta'
import { createRouter } from './router.js'
import NoSSR from './components/no-ssr.js'
import NuxtChild from './components/nuxt-child.js'
import NuxtLink from './components/nuxt-link.js'
import NuxtError from '..\\layouts\\error.vue'
import Nuxt from './components/nuxt.js'
import App from './App.js'
import { setContext, getLocation, getRouteData } from './utils'
import { createStore } from './store.js'

/* Plugins */
import nuxt_plugin_googleanalytics_4214e71a from 'nuxt_plugin_googleanalytics_4214e71a' // Source: ./google-analytics.js (ssr: false)
import nuxt_plugin_axios_cfd37ce8 from 'nuxt_plugin_axios_cfd37ce8' // Source: ./axios.js
import nuxt_plugin_vuetify_165b1c58 from 'nuxt_plugin_vuetify_165b1c58' // Source: ..\\plugins\\vuetify
import nuxt_plugin_axios_3566aa80 from 'nuxt_plugin_axios_3566aa80' // Source: ..\\plugins\\axios
import nuxt_plugin_lodash_5d4c7b8c from 'nuxt_plugin_lodash_5d4c7b8c' // Source: ..\\plugins\\lodash.js
import nuxt_plugin_fontawesome_b8db358e from 'nuxt_plugin_fontawesome_b8db358e' // Source: ..\\plugins\\fontawesome.js
import nuxt_plugin_moment_57893d7e from 'nuxt_plugin_moment_57893d7e' // Source: ..\\plugins\\moment.js
import nuxt_plugin_clipboard_c6a8f1d2 from 'nuxt_plugin_clipboard_c6a8f1d2' // Source: ..\\plugins\\clipboard.js
import nuxt_plugin_helpers_1c816d70 from 'nuxt_plugin_helpers_1c816d70' // Source: ..\\plugins\\helpers.js
import nuxt_plugin_carousel_2496c93e from 'nuxt_plugin_carousel_2496c93e' // Source: ..\\plugins\\carousel.js (ssr: false)
import nuxt_plugin_juxtapose_d62fb57c from 'nuxt_plugin_juxtapose_d62fb57c' // Source: ..\\plugins\\juxtapose.js (ssr: false)
import nuxt_plugin_infiniteloading_80126b76 from 'nuxt_plugin_infiniteloading_80126b76' // Source: ..\\plugins\\infiniteloading.js (ssr: false)
import nuxt_plugin_facebookSDK_120a0ed9 from 'nuxt_plugin_facebookSDK_120a0ed9' // Source: ..\\plugins\\facebookSDK.js (ssr: false)
import nuxt_plugin_mapbox_80e2041c from 'nuxt_plugin_mapbox_80e2041c' // Source: ..\\plugins\\mapbox.js (ssr: false)
import nuxt_plugin_scrollama_4ddd5ba5 from 'nuxt_plugin_scrollama_4ddd5ba5' // Source: ..\\plugins\\scrollama.js (ssr: false)
import nuxt_plugin_typeform_60d94103 from 'nuxt_plugin_typeform_60d94103' // Source: ..\\plugins\\typeform.js (ssr: false)
import nuxt_plugin_facebookPixel_7535da0d from 'nuxt_plugin_facebookPixel_7535da0d' // Source: ..\\plugins\\facebookPixel.js (ssr: false)
import nuxt_plugin_hotjar_6900f5d3 from 'nuxt_plugin_hotjar_6900f5d3' // Source: ..\\plugins\\hotjar.js (ssr: false)


// Component: <no-ssr>
Vue.component(NoSSR.name, NoSSR)

// Component: <nuxt-child>
Vue.component(NuxtChild.name, NuxtChild)

// Component: <nuxt-link>
Vue.component(NuxtLink.name, NuxtLink)

// Component: <nuxt>`
Vue.component(Nuxt.name, Nuxt)

// vue-meta configuration
Vue.use(Meta, {
  keyName: 'head', // the component option name that vue-meta looks for meta info on.
  attribute: 'data-n-head', // the attribute name vue-meta adds to the tags it observes
  ssrAttribute: 'data-n-head-ssr', // the attribute name that lets vue-meta know that meta info has already been server-rendered
  tagIDKeyName: 'hid' // the property name that vue-meta uses to determine whether to overwrite or append a tag
})

const defaultTransition = {"name":"page","mode":"out-in","appear":false,"appearClass":"appear","appearActiveClass":"appear-active","appearToClass":"appear-to"}

async function createApp (ssrContext) {
  const router = createRouter(ssrContext)

  
  const store = createStore(ssrContext)
  // Add this.$router into store actions/mutations
  store.$router = router
  

  // Create Root instance
  // here we inject the router and store to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = {
    router,
    store,
    nuxt: {
      defaultTransition,
      transitions: [ defaultTransition ],
      setTransitions (transitions) {
        if (!Array.isArray(transitions)) {
          transitions = [ transitions ]
        }
        transitions = transitions.map((transition) => {
          if (!transition) {
            transition = defaultTransition
          } else if (typeof transition === 'string') {
            transition = Object.assign({}, defaultTransition, { name: transition })
          } else {
            transition = Object.assign({}, defaultTransition, transition)
          }
          return transition
        })
        this.$options.nuxt.transitions = transitions
        return transitions
      },
      err: null,
      dateErr: null,
      error (err) {
        err = err || null
        app.context._errored = !!err
        if (typeof err === 'string') err = { statusCode: 500, message: err }
        const nuxt = this.nuxt || this.$options.nuxt
        nuxt.dateErr = Date.now()
        nuxt.err = err
        // Used in lib/server.js
        if (ssrContext) ssrContext.nuxt.error = err
        return err
      }
    },
    ...App
  }
  
  // Make app available into store via this.app
  store.app = app
  
  const next = ssrContext ? ssrContext.next : location => app.router.push(location)
  // Resolve route
  let route
  if (ssrContext) {
    route = router.resolve(ssrContext.url).route
  } else {
    const path = getLocation(router.options.base)
    route = router.resolve(path).route
  }

  // Set context to app.context
  await setContext(app, {
    route,
    next,
    error: app.nuxt.error.bind(app),
    store,
    payload: ssrContext ? ssrContext.payload : undefined,
    req: ssrContext ? ssrContext.req : undefined,
    res: ssrContext ? ssrContext.res : undefined,
    beforeRenderFns: ssrContext ? ssrContext.beforeRenderFns : undefined
  })

  const inject = function (key, value) {
    if (!key) throw new Error('inject(key, value) has no key provided')
    if (!value) throw new Error('inject(key, value) has no value provided')
    key = '$' + key
    // Add into app
    app[key] = value
    
    // Add into store
    store[key] = app[key]
    
    // Check if plugin not already installed
    const installKey = '__nuxt_' + key + '_installed__'
    if (Vue[installKey]) return
    Vue[installKey] = true
    // Call Vue.use() to install the plugin into vm
    Vue.use(() => {
      if (!Vue.prototype.hasOwnProperty(key)) {
        Object.defineProperty(Vue.prototype, key, {
          get () {
            return this.$root.$options[key]
          }
        })
      }
    })
  }

  
  if (process.browser) {
    // Replace store state before plugins execution
    if (window.__NUXT__ && window.__NUXT__.state) {
      store.replaceState(window.__NUXT__.state)
    }
  }
  

  // Plugin execution
  
  if (typeof nuxt_plugin_axios_cfd37ce8 === 'function') await nuxt_plugin_axios_cfd37ce8(app.context, inject)
  if (typeof nuxt_plugin_vuetify_165b1c58 === 'function') await nuxt_plugin_vuetify_165b1c58(app.context, inject)
  if (typeof nuxt_plugin_axios_3566aa80 === 'function') await nuxt_plugin_axios_3566aa80(app.context, inject)
  if (typeof nuxt_plugin_lodash_5d4c7b8c === 'function') await nuxt_plugin_lodash_5d4c7b8c(app.context, inject)
  if (typeof nuxt_plugin_fontawesome_b8db358e === 'function') await nuxt_plugin_fontawesome_b8db358e(app.context, inject)
  if (typeof nuxt_plugin_moment_57893d7e === 'function') await nuxt_plugin_moment_57893d7e(app.context, inject)
  if (typeof nuxt_plugin_clipboard_c6a8f1d2 === 'function') await nuxt_plugin_clipboard_c6a8f1d2(app.context, inject)
  if (typeof nuxt_plugin_helpers_1c816d70 === 'function') await nuxt_plugin_helpers_1c816d70(app.context, inject)
  
  if (process.browser) { 
    if (typeof nuxt_plugin_googleanalytics_4214e71a === 'function') await nuxt_plugin_googleanalytics_4214e71a(app.context, inject)
    if (typeof nuxt_plugin_carousel_2496c93e === 'function') await nuxt_plugin_carousel_2496c93e(app.context, inject)
    if (typeof nuxt_plugin_juxtapose_d62fb57c === 'function') await nuxt_plugin_juxtapose_d62fb57c(app.context, inject)
    if (typeof nuxt_plugin_infiniteloading_80126b76 === 'function') await nuxt_plugin_infiniteloading_80126b76(app.context, inject)
    if (typeof nuxt_plugin_facebookSDK_120a0ed9 === 'function') await nuxt_plugin_facebookSDK_120a0ed9(app.context, inject)
    if (typeof nuxt_plugin_mapbox_80e2041c === 'function') await nuxt_plugin_mapbox_80e2041c(app.context, inject)
    if (typeof nuxt_plugin_scrollama_4ddd5ba5 === 'function') await nuxt_plugin_scrollama_4ddd5ba5(app.context, inject)
    if (typeof nuxt_plugin_typeform_60d94103 === 'function') await nuxt_plugin_typeform_60d94103(app.context, inject)
    if (typeof nuxt_plugin_facebookPixel_7535da0d === 'function') await nuxt_plugin_facebookPixel_7535da0d(app.context, inject)
    if (typeof nuxt_plugin_hotjar_6900f5d3 === 'function') await nuxt_plugin_hotjar_6900f5d3(app.context, inject)
  }

  // If server-side, wait for async component to be resolved first
  if (process.server && ssrContext && ssrContext.url) {
    await new Promise((resolve, reject) => {
      router.push(ssrContext.url, resolve, () => {
        // navigated to a different route in router guard
        const unregister = router.afterEach(async (to, from, next) => {
          ssrContext.url = to.fullPath
          app.context.route = await getRouteData(to)
          app.context.params = to.params || {}
          app.context.query = to.query || {}
          unregister()
          resolve()
        })
      })
    })
  }

  return {
    app,
    router,
    store
  }
}

export { createApp, NuxtError }
