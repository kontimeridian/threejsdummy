import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _e962517e = () => import('..\\pages\\terms-of-use\\index.vue' /* webpackChunkName: "pages_terms-of-use_index" */).then(m => m.default || m)
const _170710ae = () => import('..\\pages\\stories\\index.vue' /* webpackChunkName: "pages_stories_index" */).then(m => m.default || m)
const _60569841 = () => import('..\\pages\\privacy-policy\\index.vue' /* webpackChunkName: "pages_privacy-policy_index" */).then(m => m.default || m)
const _05ef41bc = () => import('..\\pages\\contact\\index.vue' /* webpackChunkName: "pages_contact_index" */).then(m => m.default || m)
const _00d360af = () => import('..\\pages\\about\\index.vue' /* webpackChunkName: "pages_about_index" */).then(m => m.default || m)
const _3d9449cc = () => import('..\\pages\\services\\index.vue' /* webpackChunkName: "pages_services_index" */).then(m => m.default || m)
const _58843e07 = () => import('..\\pages\\careers\\index.vue' /* webpackChunkName: "pages_careers_index" */).then(m => m.default || m)
const _1c402c00 = () => import('..\\pages\\map\\index.vue' /* webpackChunkName: "pages_map_index" */).then(m => m.default || m)
const _4021eb50 = () => import('..\\pages\\stories\\hajj\\index.vue' /* webpackChunkName: "pages_stories_hajj_index" */).then(m => m.default || m)
const _31b1d636 = () => import('..\\pages\\stories\\singapore-response-wuhan-coronavirus-timeline\\index.vue' /* webpackChunkName: "pages_stories_singapore-response-wuhan-coronavirus-timeline_index" */).then(m => m.default || m)
const _7ae97e28 = () => import('..\\pages\\stories\\singapore-where-got-ghost\\index.vue' /* webpackChunkName: "pages_stories_singapore-where-got-ghost_index" */).then(m => m.default || m)
const _5b8d6c17 = () => import('..\\pages\\shop\\belt-road-initiative.vue' /* webpackChunkName: "pages_shop_belt-road-initiative" */).then(m => m.default || m)
const _1085ce72 = () => import('..\\pages\\stories\\we-are-just-that-into-mie-asia-s-obsession-with-instant-noodles\\index.vue' /* webpackChunkName: "pages_stories_we-are-just-that-into-mie-asia-s-obsession-with-instant-noodles_index" */).then(m => m.default || m)
const _7dfa0542 = () => import('..\\pages\\stories\\singapore-bicentennial-colonial-history-19th-century-data\\index.vue' /* webpackChunkName: "pages_stories_singapore-bicentennial-colonial-history-19th-century-data_index" */).then(m => m.default || m)
const _0c47eb2f = () => import('..\\pages\\stories\\singapore-coronavirus-cases-spread-connections\\index.vue' /* webpackChunkName: "pages_stories_singapore-coronavirus-cases-spread-connections_index" */).then(m => m.default || m)
const _27073abc = () => import('..\\pages\\stories\\going-under-how-sea-level-rise-is-threatening-to-sink-major-asian-cities\\index.vue' /* webpackChunkName: "pages_stories_going-under-how-sea-level-rise-is-threatening-to-sink-major-asian-cities_index" */).then(m => m.default || m)
const _8d13d9fe = () => import('..\\pages\\stories\\spoon-billed-sandpiper-migratory-bird-conservation-yellow-sea-map\\index.vue' /* webpackChunkName: "pages_stories_spoon-billed-sandpiper-migratory-bird-conservation-yellow-sea-map_index" */).then(m => m.default || m)
const _c47e4e86 = () => import('..\\pages\\stories\\asian-representation-in-movies-have-things-changed-since-1997\\index.vue' /* webpackChunkName: "pages_stories_asian-representation-in-movies-have-things-changed-since-1997_index" */).then(m => m.default || m)
const _1484d307 = () => import('..\\pages\\stories\\the-things-we-queued-for\\index.vue' /* webpackChunkName: "pages_stories_the-things-we-queued-for_index" */).then(m => m.default || m)
const _1348fd33 = () => import('..\\pages\\stories\\2020-chinese-zodiac-horoscope-predictions-based-on-data\\index.vue' /* webpackChunkName: "pages_stories_2020-chinese-zodiac-horoscope-predictions-based-on-data_index" */).then(m => m.default || m)
const _c089067c = () => import('..\\pages\\stories\\singapore-where-got-ghost\\types-of-ghosts\\index.vue' /* webpackChunkName: "pages_stories_singapore-where-got-ghost_types-of-ghosts_index" */).then(m => m.default || m)
const _66da77a5 = () => import('..\\pages\\stories\\singapore-where-got-ghost\\map-of-confessions\\index.vue' /* webpackChunkName: "pages_stories_singapore-where-got-ghost_map-of-confessions_index" */).then(m => m.default || m)
const _d9849a3a = () => import('..\\pages\\careers\\_slug\\index.vue' /* webpackChunkName: "pages_careers__slug_index" */).then(m => m.default || m)
const _207c81fe = () => import('..\\pages\\stories\\_slug\\index.vue' /* webpackChunkName: "pages_stories__slug_index" */).then(m => m.default || m)
const _7e131190 = () => import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */).then(m => m.default || m)



const scrollBehavior = function() {
      return { x: 0, y: 0 };
    }


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/terms-of-use",
			component: _e962517e,
			name: "terms-of-use"
		},
		{
			path: "/stories",
			component: _170710ae,
			name: "stories"
		},
		{
			path: "/privacy-policy",
			component: _60569841,
			name: "privacy-policy"
		},
		{
			path: "/contact",
			component: _05ef41bc,
			name: "contact"
		},
		{
			path: "/about",
			component: _00d360af,
			name: "about"
		},
		{
			path: "/services",
			component: _3d9449cc,
			name: "services"
		},
		{
			path: "/careers",
			component: _58843e07,
			name: "careers"
		},
		{
			path: "/map",
			component: _1c402c00,
			name: "map"
		},
		{
			path: "/stories/hajj",
			component: _4021eb50,
			name: "stories-hajj"
		},
		{
			path: "/stories/singapore-response-wuhan-coronavirus-timeline",
			component: _31b1d636,
			name: "stories-singapore-response-wuhan-coronavirus-timeline"
		},
		{
			path: "/stories/singapore-where-got-ghost",
			component: _7ae97e28,
			name: "stories-singapore-where-got-ghost"
		},
		{
			path: "/shop/belt-road-initiative",
			component: _5b8d6c17,
			name: "shop-belt-road-initiative"
		},
		{
			path: "/stories/we-are-just-that-into-mie-asia-s-obsession-with-instant-noodles",
			component: _1085ce72,
			name: "stories-we-are-just-that-into-mie-asia-s-obsession-with-instant-noodles"
		},
		{
			path: "/stories/singapore-bicentennial-colonial-history-19th-century-data",
			component: _7dfa0542,
			name: "stories-singapore-bicentennial-colonial-history-19th-century-data"
		},
		{
			path: "/stories/singapore-coronavirus-cases-spread-connections",
			component: _0c47eb2f,
			name: "stories-singapore-coronavirus-cases-spread-connections"
		},
		{
			path: "/stories/going-under-how-sea-level-rise-is-threatening-to-sink-major-asian-cities",
			component: _27073abc,
			name: "stories-going-under-how-sea-level-rise-is-threatening-to-sink-major-asian-cities"
		},
		{
			path: "/stories/spoon-billed-sandpiper-migratory-bird-conservation-yellow-sea-map",
			component: _8d13d9fe,
			name: "stories-spoon-billed-sandpiper-migratory-bird-conservation-yellow-sea-map"
		},
		{
			path: "/stories/asian-representation-in-movies-have-things-changed-since-1997",
			component: _c47e4e86,
			name: "stories-asian-representation-in-movies-have-things-changed-since-1997"
		},
		{
			path: "/stories/the-things-we-queued-for",
			component: _1484d307,
			name: "stories-the-things-we-queued-for"
		},
		{
			path: "/stories/2020-chinese-zodiac-horoscope-predictions-based-on-data",
			component: _1348fd33,
			name: "stories-2020-chinese-zodiac-horoscope-predictions-based-on-data"
		},
		{
			path: "/stories/singapore-where-got-ghost/types-of-ghosts",
			component: _c089067c,
			name: "stories-singapore-where-got-ghost-types-of-ghosts"
		},
		{
			path: "/stories/singapore-where-got-ghost/map-of-confessions",
			component: _66da77a5,
			name: "stories-singapore-where-got-ghost-map-of-confessions"
		},
		{
			path: "/careers/:slug",
			component: _d9849a3a,
			name: "careers-slug"
		},
		{
			path: "/stories/:slug",
			component: _207c81fe,
			name: "stories-slug"
		},
		{
			path: "/",
			component: _7e131190,
			name: "index"
		}
    ],
    
    
    fallback: false
  })
}
