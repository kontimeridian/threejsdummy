import Vuex from 'vuex';

const createStore = () =>
  new Vuex.Store({
    state: {
      meta: {
        host: '',
        canonicalURL: '',
      },
      staticPostSlugs: [
        'asian-representation-in-movies-have-things-changed-since-1997',
        'going-under-how-sea-level-rise-is-threatening-to-sink-major-asian-cities',
        'we-are-just-that-into-mie-asia-s-obsession-with-instant-noodles',
        'singapore-where-got-ghost',
        'the-things-we-queued-for',
        '2020-chinese-zodiac-horoscope-predictions-based-on-data',
        'singapore-response-wuhan-coronavirus-timeline',
        'singapore-coronavirus-cases-spread-connections',
        'singapore-bicentennial-colonial-history-19th-century-data',
        'spoon-billed-sandpiper-migratory-bird-conservation-yellow-sea-map',
        'hajj',
      ],
      previewPosts: [],
      popularPosts: [],
      viewPosts: [],
      postTemplate: '',
      postNavActive: false,
      postSheetBottom: {
        show: false,
        clicked: false,
      },
      storiesTab: -1,
      storiesDropdown: 0,
      lastPos: 0,
      isMobile: false,
    },
    mutations: {
      setMeta(state, meta) {
        state.meta = meta;
      },
      setPreviewPosts(state, posts) {
        state.previewPosts = posts;
      },
      setPopularPosts(state, posts) {
        state.popularPosts = posts;
      },
      setPostTemplate(state, postTemplate) {
        state.postTemplate = postTemplate;
      },
      togglePostNavState(state) {
        state.postNavActive = !state.postNavActive;
      },
      setPostNavState(state, postNavState) {
        state.postNavActive = postNavState;
      },
      setPostSheetBottomShow(state, postSheetBottomState) {
        state.postSheetBottom.show = postSheetBottomState;
      },
      setPostSheetBottomClick(state, postSheetBottomState) {
        state.postSheetBottom.clicked = postSheetBottomState;
        state.postSheetBottom.show = !postSheetBottomState;
      },
      setStoriesTab(state, currentTab) {
        state.storiesTab = currentTab;
      },
      setStoriesDropdown(state, currentSelection) {
        state.storiesDropdown = currentSelection;
      },
      setLastPos(state, lastPos) {
        state.lastPos = lastPos;
      },
      setIsMobile(state, isMobile) {
        state.isMobile = isMobile;
      },
    },
    actions: {
      async nuxtServerInit(vuexContext, { app, route, req }) {
        // check for x-forwarded-proto
        // set protocol
        const xForwardedProto = req.headers['x-forwarded-proto'];
        const protocol = xForwardedProto ? `${xForwardedProto}://` : 'http://';

        // set meta
        vuexContext.commit('setMeta', {
          origin: `${protocol}${req.headers.host}`,
          canonicalURL: `${protocol}${req.headers.host}${route.fullPath}`,
        });

        // get preview posts
        try {
          const previewPosts = await app.$axios.$get('preview-posts');

          vuexContext.commit('setPreviewPosts', previewPosts);
        } catch (error) {
          // handle errors
          console.log(error.response);
          vuexContext.commit('setPreviewPosts', []);
        }

        // get popular posts from GA
        try {
          const popularPosts = await app.$axios.$get('popular-posts');

          vuexContext.commit('setPopularPosts', popularPosts);
        } catch (error) {
          // handle errors
          console.log(error.response);
          vuexContext.commit('setPopularPosts', []);
        }
      },
      togglePostNav({ commit }) {
        commit('togglePostNavState');
      },
      setPostTemplate({ commit }, postTemplate) {
        commit('setPostTemplate', postTemplate);
      },
      setPostNavState({ commit }, postNavState) {
        commit('setPostNavState', postNavState);
      },
      setPostSheetBottomShow({ commit }, postSheetBottomState) {
        commit('setPostSheetBottomShow', postSheetBottomState);
      },
      setPostSheetBottomClick({ commit }, postSheetBottomState) {
        commit('setPostSheetBottomClick', postSheetBottomState);
      },
      setStoriesTab({ commit }, currentTab) {
        commit('setStoriesTab', currentTab);
      },
      setStoriesDropdown({ commit }, currentSelection) {
        commit('setStoriesDropdown', currentSelection);
      },
      setLastPos({ commit }, lastPos) {
        commit('setLastPos', lastPos);
      },
      setIsMobile({ commit }, isMobile) {
        commit('setIsMobile', isMobile);
      },
    },
    getters: {
      meta: state => state.meta,
      staticPostSlugs: state => state.staticPostSlugs,
      previewPosts: state => state.previewPosts,
      popularPosts: state => state.popularPosts,
      postNavState: state => state.postNavActive,
      postSheetBottomState: state => state.postSheetBottom,
      featuredPosts: state => state.previewPosts.filter(post => post.is_featured),
      relatedPosts: state => id => state.previewPosts.filter(post => post.categories.some(cat => cat.id === id)),
      storiesTab: state => state.storiesTab,
      storiesDropdown: state => state.storiesDropdown,
      lastPos: state => state.lastPos,
      postTemplate: state => state.postTemplate,
      isMobile: state => state.isMobile,
    },
  });

export default createStore;
