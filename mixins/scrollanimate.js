export default {
  methods: {
    onScroll(e) {
      // current scroll position
      let currentPos = 0;
      if (this.$store.getters.postTemplate === 'regular' || this.$store.getters.postTemplate === 'static') {
        // track reading progress
        currentPos = e.target.documentElement.scrollTop || e.target.body.scrollTop;
      } else if (this.$store.getters.postTemplate === 'naomi') {
        currentPos = e.target.scrollTop;
      } else if (this.$store.getters.postTemplate === 'exp') {
        // track reading progress
        currentPos = e.target.documentElement.scrollTop || e.target.body.scrollTop;
      }
      this.trackReadingProgress(e, currentPos);
      // track post navbar state
      if (this.$store.getters.isMobile) {
        return;
      }
      this.trackPostNavState(currentPos);
    },
    trackReadingProgress(e, currentPos) {
      // the container to track progress
      let progressTargetHeight = 0;
      let progress = 0;

      if (this.$store.getters.postTemplate === 'regular') {
        const singlePostHeight = document.querySelector('.single-post').scrollHeight;
        progressTargetHeight = singlePostHeight - e.target.documentElement.clientHeight;
      } else if (this.$store.getters.postTemplate === 'static') {
        const storyHeight = document.querySelector('.static-post').scrollHeight;
        progressTargetHeight = storyHeight - e.target.documentElement.clientHeight;
      } else if (this.$store.getters.postTemplate === 'naomi') {
        const singlePostHeight = document.querySelector('.single-post').scrollHeight;
        progressTargetHeight = singlePostHeight - e.target.clientHeight;
      } else {
        // exp
        const panelWrapperHeight = document.querySelector('.panel-wrapper').scrollHeight;
        if (!this.$store.getters.isMobile) {
          progressTargetHeight = panelWrapperHeight - e.target.documentElement.clientHeight;
        } else {
          const mapHeight = document.getElementById('map').clientHeight;
          progressTargetHeight = panelWrapperHeight - e.target.documentElement.clientHeight + mapHeight;
        }
      }
      progress = (currentPos / progressTargetHeight) * 100;

      if (progress >= 100 && this.$store.getters.postSheetBottomState.show) {
        this.$store.dispatch('setPostSheetBottomShow', false);
      } else if (
        progress < 100 &&
        !this.$store.getters.postSheetBottomState.show &&
        !this.$store.getters.postSheetBottomState.clicked
      ) {
        this.$store.dispatch('setPostSheetBottomShow', true);
      }
      // set the css for progress bar
      this.$nuxt.$loading.set(progress);
    },
    trackPostNavState(currentPos) {
      // detect if scrolling down or up
      const isScrollingDown = currentPos > this.$store.getters.lastPos;

      // update previous scroll position
      this.$store.dispatch('setLastPos', currentPos);

      // calculate the scroll threshold
      let scrollThreshold = 0;
      if (this.$store.getters.postTemplate === 'regular') {
        const heroHeight = document.querySelector('.single-post__hero').offsetHeight;
        const headingHeight = document.querySelector('.single-post__heading').offsetHeight;
        scrollThreshold = heroHeight + headingHeight;
      } else if (this.$store.getters.postTemplate === 'static') {
        // get the trigger point
        scrollThreshold = document.querySelector('.navbar-trigger').offsetTop;
      } else {
        // naomi & exp
        scrollThreshold = 0;
      }

      // dispatch action to update post nav state
      if (currentPos >= scrollThreshold && isScrollingDown && !this.$store.getters.postNavState) {
        this.$store.dispatch('togglePostNav');
      } else if (!isScrollingDown && this.$store.getters.postNavState) {
        this.$store.dispatch('togglePostNav');
      }
    },
  },
};
