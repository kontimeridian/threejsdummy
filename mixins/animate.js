export default {
  mounted() {
    this.isInViewport();
  },
  methods: {
    isInViewport() {
      // get viewport height
      const viewport = window.innerHeight || document.documentElement.clientHeight;

      // get all elements to animate
      const animateEls = document.querySelectorAll('.animated');

      // loop through elements
      animateEls.forEach(el => {
        // get the element scroll top position
        const elTop = el.getBoundingClientRect().top;

        if (elTop < viewport) {
          el.classList.add('fadeInUp', 'slow');
        }
      });
    },
  },
};
