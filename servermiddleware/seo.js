const redirects = [
  {
    from: '/blog/dam-if-you-do-dam-if-you-dont-hydropower-in-the-mekong',
    to: '/stories/dam-if-you-do-dam-if-you-dont-hydropower-in-the-mekong',
  },
  {
    from: '/blog/why-is-naming-jerusalem-the-capital-of-israel-problematic',
    to: '/stories/why-is-naming-jerusalem-the-capital-of-israel-problematic',
  },
  {
    from: '/blog/why-is-this-great-southeast-asian-hero-not-recognized-in-west-java',
    to: '/stories/why-is-this-great-southeast-asian-hero-not-recognized-in-west-java',
  },
  {
    from: '/blog/how-does-rice-get-to-your-plate',
    to: '/stories/how-does-rice-get-to-your-plate',
  },
  {
    from: '/blog/whats-killing-the-sharks-of-the-persian-gulf',
    to: '/stories/whats-killing-the-sharks-of-the-persian-gulf',
  },
  {
    from: '/blog/east-vs-west-the-truth-about-the-shark-trade',
    to: '/stories/east-vs-west-the-truth-about-the-shark-trade',
  },
  {
    from: '/blog/plastics-oceans-and-human-health',
    to: '/stories/plastics-oceans-and-human-health',
  },
  {
    from: '/blog/rohingya-in-rakhine-who-are-they-and-what-is-their-history',
    to: '/stories/rohingya-in-rakhine-who-are-they-and-what-is-their-history',
  },
  {
    from: '/blog/how-maps-create-nations',
    to: '/stories/how-maps-create-nations',
  },
  {
    from: '/blog/forests-by-the-sea-mangroves-and-why-they-are-threatened',
    to: '/stories/forests-by-the-sea-mangroves-and-why-they-are-threatened',
  },
  {
    from: '/blog/the-real-cost-of-seafood',
    to: '/stories/the-real-cost-of-seafood',
  },
  {
    from: '/blog/languages-of-india-and-indonesia',
    to: '/stories/languages-of-india-and-indonesia',
  },
  {
    from: '/blog/asia-connected',
    to: '/stories/asia-connected',
  },
  {
    from: '/blog/palm-oil-environmental-costs-and-the-problems-behind-lobbying',
    to: '/stories/palm-oil-environmental-costs-and-the-problems-behind-lobbying',
  },
  {
    from: '/blog/how-asia-understands-democracy-through-pop-culture',
    to: '/stories/how-asia-understands-democracy-through-pop-culture',
  },
  {
    from: '/partnerships',
    to: '/services',
  },
  {
    from: '/stories/the-tibetan-plateau-melting-the-roof-of-the-world',
    to: '/stories/climate-change-is-melting-the-tibetan-plateau',
  },
];

module.exports = function(req, res, next) {
  const redirect = redirects.find(r => r.from === req.url);

  if (redirect) {
    res.writeHead(301, { Location: redirect.to });
    res.end();
  } else {
    next();
  }
};
