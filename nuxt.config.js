// load env variables
require('dotenv').config();

const nodeExternals = require('webpack-node-externals');
const sitemap = require('./plugins/sitemap.js');
const path = require('path');

module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: titleChunk => (titleChunk ? `${titleChunk} | Kontinentalist` : 'Kontinentalist'),
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Mapping Stories, Bridging Gaps.' },
      // favicons
      { name: 'theme-color', content: '#ffffff' },
      { name: 'msapplication-TileColor', content: '#da532c' },
      // open graph meta
      { hid: 'fb:app_id', property: 'fb:app_id', content: '311767232690011' },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'og:site_name', property: 'og:site_name', content: 'Kontinentalist' },
      // twitter card
      { hid: 'twitter:card', name: 'twitter:card', content: 'summary_large_image' },
      { hid: 'twitter:site', name: 'twitter:site', content: '@konti_sg' },
    ],
    script: [
      { src: 'https://connect.facebook.net/en_US/sdk.js', async: true },
      { src: 'https://www.youtube.com/iframe_api', async: true },
    ],
    link: [
      // favicons
      { rel: 'manifest', href: '/data/manifest.webmanifest' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/favicons/apple-touch-icon.png' },
      { rel: 'icon', sizes: '32x32', href: '/favicons/favicon-32x32.png' },
      { rel: 'icon', sizes: '16x16', href: '/favicons/favicon-16x16.png' },
      { rel: 'mask-icon', color: '#5bbab5', href: '/favicons/safari-pinned-tab.svg' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicons/favicon.ico' },
      // material icons
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Material+Icons' },
    ],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#F2784B' },

  /*
  ** Global CSS
  */
  css: ['~/assets/styles/main.styl'],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    // external packages
    '~/plugins/vuetify',
    '~/plugins/axios',
    '~/plugins/lodash.js',
    '~/plugins/fontawesome.js',
    // libraries
    '~/plugins/moment.js',
    '~/plugins/clipboard.js',
    '~/plugins/helpers.js',
    { src: '~/plugins/carousel.js', ssr: false },
    { src: '~/plugins/juxtapose.js', ssr: false },
    { src: '~/plugins/infiniteloading.js', ssr: false },
    // { src: '~/plugins/three.js', ssr: false },
    // sdks
    { src: '~/plugins/facebookSDK.js', ssr: false },
    { src: '~/plugins/mapbox.js', ssr: false },
    { src: '~/plugins/scrollama.js', ssr: false },
    { src: '~/plugins/typeform.js', ssr: false },
    // seo, marketing
    { src: '~/plugins/facebookPixel.js', ssr: false },
    { src: '~/plugins/hotjar.js', ssr: false },
  ],

  /*
  ** Server middleware
  */
  serverMiddleware: ['~/servermiddleware/seo.js'],

  /*
  ** Nuxt.js modules
  */
  modules: ['@nuxtjs/dotenv', '@nuxtjs/axios', '@nuxtjs/google-analytics', '@nuxtjs/sitemap'],

  /**
   * @nuxtjs/sitemap module configuration
   */
  sitemap,

  /*
  ** Axios module configuration
  */
  axios: {},

  /*
  ** Analytics module configuration
  */
  'google-analytics': {
    id: 'UA-119672452-1',
    debug: {
      sendHitTask: process.env.NODE_ENV === 'live',
    },
  },

  /*
  ** Environment variables shared for client & server-side
  */
  env: {
    API_TOKEN: process.env.API_TOKEN,
    MAPBOX_TOKEN: process.env.MAPBOX_TOKEN,
  },

  /*
  ** Customize nuxtjs router
  */
  router: {
    scrollBehavior() {
      return { x: 0, y: 0 };
    },
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** Include in vendor bundle for better caching
    */
    vendor: [
      // external packages
      '~/plugins/lodash.js',
      '~/plugins/fontawesome.js',
      // libraries
      '~/plugins/moment.js',
      '~/plugins/clipboard.js',
      '~/plugins/juxtapose.js',
      '~/plugins/infiniteloading.js',
      'three'
    ],

    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      // if (ctx.isDev && ctx.isClient) {
      //   config.module.rules.push({
      //     enforce: 'pre',
      //     test: /\.(js|vue)$/,
      //     loader: 'eslint-loader',
      //     exclude: /(node_modules)/,
      //   });
      // }
      if (ctx.isServer) {
        config.externals = [
          nodeExternals({
            whitelist: [/^vuetify/],
          }),
        ];
        config.module.rules.push({
          test: /\.js$/,
          include: [
            path.resolve(__dirname, 'node_modules/three')
          ],
          loader: 'babel-loader',
          options: this.getBabelOptions(ctx.isServer)
        })
      }
    },
  },
};
