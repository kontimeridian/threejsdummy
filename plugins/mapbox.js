import Vue from 'vue';
import mapboxgl from 'mapbox-gl';

// declare mapbox helpers
const MapboxHelpers = {
  load() {
    return new Promise((resolve, reject) => {
      mapboxgl.accessToken = process.env.MAPBOX_TOKEN;

      if (typeof mapboxgl === 'undefined') {
        return reject;
      }

      return resolve('Map loaded');
    });
  },
  constructMap(params = {}) {
    return new mapboxgl.Map(params);
  },
  navigationControl(params = {}) {
    return new mapboxgl.NavigationControl(params);
  },
  marker(params = {}) {
    return new mapboxgl.Marker(params);
  },
  popup(params = {}) {
    return new mapboxgl.Popup(params);
  },
};

// register mapbox helpers
Object.defineProperty(Vue.prototype, '$Mapbox', { value: MapboxHelpers });
