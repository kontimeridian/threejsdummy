import Vue from 'vue';

const helpersMixin = {
  methods: {
    injectNewTabToLinks() {
      const $aTags = this.$el.getElementsByTagName('a');
      for (let i = 0; i < $aTags.length; i++) {
        $aTags[i].setAttribute('target', '_blank');
      }
    },
    cConsole(string) {
      // eslint-disable-next-line no-console
      console.log(`%c${string}`, 'background: #222; color: #bada55');
    },
  },
};

Vue.mixin(helpersMixin);
