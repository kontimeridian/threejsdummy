import Vue from 'vue';
import Vuetify from 'vuetify';
// import colors from 'vuetify/es5/util/colors';

Vue.use(Vuetify, {
  theme: {
    primary: '#F2784B', // a color that is not in the material colors palette
    secondary: '#FAF9F7',
    'dark-primary': '#DAD4BB',
  },
  iconFont: 'fa',
});
