/* global FB */
/* global fbq */
import Vue from 'vue';

// declare fb helpers
const FBhelpers = {
  load() {
    return new Promise((resolve, reject) => {
      // create script
      const head = document.head || document.getElementsByTagName('head')[0];
      const script = document.createElement('script');
      script.src = 'https://connect.facebook.net/en_US/sdk.js';
      script.id = 'fbSDK';
      script.async = true;

      head.appendChild(script);

      script.onerror = reject;

      resolve('FB loaded');
    });
  },
  init() {
    FB.init({
      appId: '311767232690011',
      autoLogAppEvents: true,
      xfbml: true,
      version: 'v3.0',
    });
  },
  share(url) {
    FB.ui({
      method: 'share',
      href: url,
    });
  },
  pixelTrack(eventName) {
    fbq('track', eventName);
  },
};

// register fb helpers
Object.defineProperty(Vue.prototype, '$FB', { value: FBhelpers });
