import Vue from 'vue';
import * as typeformEmbed from '@typeform/embed';

// register mapbox helpers
Object.defineProperty(Vue.prototype, '$typeformEmbed', { value: typeformEmbed });
