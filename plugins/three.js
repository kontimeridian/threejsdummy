import Vue from 'vue'
import * as THREE from 'three'
import 'imports-loader?THREE=three!three/examples/js/loaders/OBJLoader.js'
import 'imports-loader?THREE=three!three/examples/js/loaders/MTLLoader.js'
import 'imports-loader?THREE=three!three/examples/js/controls/OrbitControls.js'
import 'imports-loader?THREE=three!three/examples/jsm/loaders/obj2/bridge/MtlObjBridge.js'
import 'imports-loader?THREE=three!three/examples/jsm/loaders/OBJLoader2.js'

Vue.use({
  install (Vue, options) {
    Vue.prototype.$THREE = THREE
  }
})