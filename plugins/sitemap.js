const axios = require('axios');

const prioritisedRoutes = [
  {
    url: '',
    changefreq: 'daily',
    priority: 1,
    lastmodISO: new Date().toISOString(),
  },
  {
    url: '/stories',
    changefreq: 'weekly',
    priority: 0.9,
    lastmodISO: new Date().toISOString(),
  },
];

const pageRoutes = [
  {
    url: '/about',
    changefreq: 'monthly',
    priority: 0.8,
    lastmodISO: '2019-01-21T13:30:00.000Z',
  },
  {
    url: '/careers',
    changefreq: 'monthly',
    priority: 0.8,
    lastmodISO: '2019-01-21T13:30:00.000Z',
  },
  {
    url: '/map',
    changefreq: 'monthly',
    priority: 0.8,
    lastmodISO: '2019-01-21T13:30:00.000Z',
  },
  {
    url: '/services',
    changefreq: 'monthly',
    priority: 0.8,
    lastmodISO: '2019-01-21T13:30:00.000Z',
  },
  {
    url: '/shop/belt-road-initiative',
    changefreq: 'monthly',
    priority: 0.8,
    lastmodISO: '2019-01-21T13:30:00.000Z',
  },
  {
    url: '/privacy-policy',
    changefreq: 'monthly',
    priority: 0.5,
    lastmodISO: '2019-01-21T13:30:00.000Z',
  },
  {
    url: '/terms-of-service',
    changefreq: 'monthly',
    priority: 0.5,
    lastmodISO: '2019-01-21T13:30:00.000Z',
  },
];

module.exports = {
  path: '/sitemap.xml',
  hostname: 'https://kontinentalist.com',
  cacheTime: 1000 * 60 * 15, // Defines how frequently should sitemap routes being updated (15 minutes).
  gzip: true,
  generate: false, // Enable me when using nuxt generate
  exclude: ['/', '/stories/index-old'],
  routes() {
    return axios
      .get(`${process.env.API_URL}preview-posts`, {
        headers: {
          Authorization: `Bearer ${process.env.API_TOKEN}`,
        },
      })
      .then(res => {
        const routes = res.data.map(post => {
          const mappedPost = {
            url: `/stories/${post.slug}`,
            changefreq: 'monthly',
            priority: 0.9,
            lastmodISO: new Date(post.modified_date).toISOString(),
          };
          return mappedPost;
        });

        return [...prioritisedRoutes, ...routes, ...pageRoutes];
      })
      .catch(err => [...prioritisedRoutes, ...pageRoutes]);
  },
};
