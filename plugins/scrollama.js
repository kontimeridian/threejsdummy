import Vue from 'vue';
import 'intersection-observer';
import scrollama from 'scrollama';

// declare scrollama helpers
const ScrollamaHelpers = {
  load() {
    return scrollama();
  },
};

// register the helpers
Object.defineProperty(Vue.prototype, '$Scrollama', { value: ScrollamaHelpers });
