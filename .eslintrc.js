module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    'plugin:vue/recommended',
    'airbnb-base',
    'plugin:prettier/recommended'
  ],
  // required to lint *.vue files
  plugins: [
    'vue'
  ],
  // eslint shared settings
  settings: {
    'import/resolver': {
      webpack: {
        config: {
          resolve: {
            extensions: ['.js', '.vue', '.json'],
            alias: {
              '~': __dirname,
              '@': __dirname,
            }
          }
        }
      }
    },
    // packages already included in nuxt or nuxt modules
    'import/core-modules': [ 'vue', 'vuex', 'webpack-node-externals', 'dotenv' ],
  },
  // add your custom rules here
  rules: {
    // allow debugger during development
    'no-debugger': 'off',
    "no-underscore-dangle": "off",
    "no-unused-vars": [
      1,
      {
        "argsIgnorePattern": "res|next|^err"
      }
    ],
    // disallow reassignment of function parameters
    // disallow parameter object manipulation except for specific exclusions
    'no-param-reassign': ['error', {
      props: true,
      ignorePropertyModificationsFor: [
        'config', // for nuxt.config.js
        'state', // for vuex state
        'acc', // for reduce accumulators
        'e', // for e.returnvalue
        'el', // for DOM elements
        'd', // for formating data (D3.js)
      ]
    }],
    'no-plusplus': 'off',

    // vue rules
    "vue/max-attributes-per-line": [2, {
      "singleline": 2,
    }],

    // prettier config
    'prettier/prettier': [
      'error',
      {
        'trailingComma': 'es5',
        'singleQuote': true,
        'printWidth': 120,
      }
    ],
  }
}
